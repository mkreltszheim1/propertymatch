Real Estate agencies have a few different ways of advertising their properties. One of them is to send us an XML file with the property details. In this scenario, we will assume we have to match the properties they are sending with properties already present in our database. This will be used to determine if a property needs to be created or updated (we actually have other information in the XML file to determine this, but let's assume it's not there for this exercise). We perform some clean-up on the agency's data before importing it, meaning that we can't just match the data "as-is". 
Let's assume we have 3 agencies using this system (we may extend to more in the future), each with different property matching rules: 

1.	Only The Best Real Estate! (AgencyCode: OTBRE) Only The Best Real Estate! use a lot of punctuation in their property names and addresses. A property is considered to be a match if both the property name and address match when punctuation is excluded. For example, the property "*Super*-High! APARTMENTS (Sydney)" located at "32 Sir John-Young Crescent, Sydney, NSW." would be a match for a property called "Super High Apartments, Sydney" located at "32 Sir John Young Crescent, Sydney NSW".

2.	 Location Real Estate (AgencyCode: LRE) Location Real Estate are not really good at locating properties, and as a result their properties will often appear to be up to 200 metres away from the actual location when placed on a map. A property is considered to be a match if the agency code is the same and the property is within 200 metres or less of the actual property location. For the sake of simplicity, assume that 1 degree of latitude or longitude is equal to 111km. 

3.	Contrary Real Estate (AgencyCode: CRE) Contrary Real Estate like to have their property names backwards. A property is considered a match if the names match when the words in the name of the property are reversed. For example, a property with the name "Apartments Summit The" is a match for a property with the name "The Summit Apartments". Given the following class: 

public class Property { 
public string Address { get; set; } 
public string AgencyCode { get; set; } 
public string Name { get; set; } 
public decimal Latitude { get; set; } 
public decimal Longitude { get; set; } 
} 
Implement this interface to determine whether a particular property from any of the agencies' inventory is a match for a property from our inventory: 

public interface IPropertyMatcher { 
bool IsMatch(Property agencyProperty, Property databaseProperty);
}