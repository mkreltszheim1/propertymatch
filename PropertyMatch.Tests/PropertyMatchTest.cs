﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PropertyMatch.Tests
{
    [TestClass]
    public class PropertyMatchTest
    {
        [TestMethod]
        public void MatchingNameAndAddress()
        {
            var agencyProperty = new Property()
            {
                Name = "*Super*-High! APARTMENTS (Sydney)",
                Address = "32 Sir John-Young Crescent, Sydney, NSW."
            };
            var databaseProperty = new Property()
            {
                Name = "Super High Apartments, Sydney",
                Address = "32 Sir John Young Crescent, Sydney NSW"
            };

            var propertyMatcher = new PropertyMatcher();
            var match = propertyMatcher.NameAndAddressMatchExcludePunctuation(agencyProperty, databaseProperty);
            Assert.IsTrue(match);
        }

        [TestMethod]
        public void MatchingReversePropertyNameEstate()
        {
            var agencyPropertyName = "The Summit Apartments";
            var databasePropertyName = "Apartments Summit The";

            var propertyMatcher = new PropertyMatcher();
            var match = propertyMatcher.NamesReverseMatch(agencyPropertyName, databasePropertyName);
            Assert.IsTrue(match);
        }

        [TestMethod]
        public void LocationWithinLimit()
        {
            var agencyProperty = new Property()
            {
                Latitude = 100,
                Longitude = 200
            };
            var databaseProperty = new Property()
            {
                Latitude = 101,
                Longitude = 200
            };

            var propertyMatcher = new PropertyMatcher();
            var match = propertyMatcher.LocationMatch(agencyProperty, databaseProperty);
            Assert.IsTrue(match);
        }

        [TestMethod]
        public void LocationOutsideLimit()
        {
            var agencyProperty = new Property()
            {
                Latitude = 100,
                Longitude = -200
            };
            var databaseProperty = new Property()
            {
                Latitude = -100,
                Longitude = 200
            };

            var propertyMatcher = new PropertyMatcher();
            var match = propertyMatcher.LocationMatch(agencyProperty, databaseProperty);
            Assert.IsFalse(match);
        }
    }
}
