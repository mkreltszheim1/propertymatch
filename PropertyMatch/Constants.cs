﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertyMatch
{
    public static class Constants
    {
        public const string AgencyCodeOnlyTheBest = "OTBRE";
        public const string AgencyCodeLocationRealEstate = "LRE";
        public const string AgencyCodeContraryRealEstate = "CRE";
        public static  List<string> AgencyCodes = new List<string>() { AgencyCodeOnlyTheBest, AgencyCodeLocationRealEstate, AgencyCodeContraryRealEstate };
        public const decimal KilometresPerDegree = 111;
        public const decimal KilometresBetweenMatchingProperties = 200;
    }
}
