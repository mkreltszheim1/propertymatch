﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace PropertyMatch
{
    public class PropertyMatcher : IPropertyMatcher
    {
        public bool IsValidAgencyCode(string agencyCode)
        {
            if (agencyCode == null)
                return false;

            return Constants.AgencyCodes.Contains(agencyCode);
        }

        public bool IsMatch(Property agencyProperty, Property databaseProperty)
        {

            if (!IsValidAgencyCode(agencyProperty.AgencyCode))
                return false;

            var returnValue = false;

            switch (agencyProperty.AgencyCode)
            {
                case Constants.AgencyCodeOnlyTheBest:
                    returnValue = NameAndAddressMatchExcludePunctuation(agencyProperty, databaseProperty);
                    break;
                case Constants.AgencyCodeLocationRealEstate:
                    returnValue = LocationMatch(agencyProperty, databaseProperty);
                    break;
                case Constants.AgencyCodeContraryRealEstate:
                    returnValue = NamesReverseMatch(agencyProperty.Name, databaseProperty.Name);
                    break;
            }

            return returnValue;
        }

        public bool NameAndAddressMatchExcludePunctuation(Property agencyProperty, Property databaseProperty)
        {
            agencyProperty.Name = StripPunctuation(agencyProperty.Name);
            agencyProperty.Address = StripPunctuation(agencyProperty.Address);

            databaseProperty.Name = StripPunctuation(databaseProperty.Name);
            databaseProperty.Address = StripPunctuation(databaseProperty.Address);

            return agencyProperty.Name == databaseProperty.Name && agencyProperty.Address == databaseProperty.Address;
        }

        public bool LocationMatch(Property agencyProperty, Property databaseProperty)
        {
            // Check that agency codes match
            if (agencyProperty.AgencyCode != databaseProperty.AgencyCode)
                return false;

            var degreesOfDifference = Math.Abs(agencyProperty.Latitude - databaseProperty.Latitude) +
                                      Math.Abs(agencyProperty.Longitude - databaseProperty.Longitude);
            var kilometresDifference = degreesOfDifference * Constants.KilometresPerDegree;
            return kilometresDifference <= Constants.KilometresBetweenMatchingProperties;
        }

        public bool NamesReverseMatch(string agencyPropertyName, string databasePropertyName)
        {
            agencyPropertyName = agencyPropertyName.Trim().ToLower();
            databasePropertyName = databasePropertyName.Trim().ToLower();

            var databasePropertyNameReversed = string.Join(" ", databasePropertyName.Split(' ').Reverse());
            return (agencyPropertyName == databasePropertyNameReversed);
        }

        public string StripPunctuation(string input)
        {
            return new string(input.Where(c => !char.IsPunctuation(c)).ToArray()).Replace(" ", "").ToLower();
        }
    }

    
}
